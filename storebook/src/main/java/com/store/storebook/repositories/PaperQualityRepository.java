package com.store.storebook.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.store.storebook.model.PaperQuality;

@Repository
public interface PaperQualityRepository extends JpaRepository<PaperQuality, Long>{

}
