package com.store.storebook.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.store.storebook.model.Book;

@Repository
public interface BookRepository extends JpaRepository<Book, Long>{

}
