package com.store.storebook.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.store.storebook.model.Author;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Long>{

}

