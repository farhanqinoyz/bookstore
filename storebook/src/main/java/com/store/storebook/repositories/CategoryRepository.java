package com.store.storebook.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.store.storebook.model.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long>{

}
