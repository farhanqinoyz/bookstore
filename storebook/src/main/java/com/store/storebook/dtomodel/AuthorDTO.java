package com.store.storebook.dtomodel;

public class AuthorDTO {
	private Long authorId;
	private Long age;
	private String country;
	private String firstName;
	private String lastName;
	
	public AuthorDTO() {
		
	}
	
	public AuthorDTO(Long authorId, Long age, String country, String firstName, String lastName) {
		super();
		this.authorId = authorId;
		this.age = age;
		this.country = country;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public Long getAge() {
		return age;
	}

	public void setAge(Long age) {
		this.age = age;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	
	
}
