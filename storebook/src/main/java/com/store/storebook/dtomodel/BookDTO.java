package com.store.storebook.dtomodel;

import java.util.Date;

import com.store.storebook.model.Author;
import com.store.storebook.model.Category;
import com.store.storebook.model.Publisher;

public class BookDTO {
	private long bookId;
	private AuthorDTO authorDTO;
	private CategoryDTO categoryDTO;
	private PublisherDTO publisherDTO;
	private String bookTitle;
	private Date releaseDate;
	private Double baseProduction;
	private Double bookPrice;
	
	public BookDTO() {
		
	}


	public BookDTO(long bookId, AuthorDTO authorDTO, CategoryDTO categoryDTO, PublisherDTO publisherDTO,
			String bookTitle, Date releaseDate, Double baseProduction, Double bookPrice) {
		super();
		this.bookId = bookId;
		this.authorDTO = authorDTO;
		this.categoryDTO = categoryDTO;
		this.publisherDTO = publisherDTO;
		this.bookTitle = bookTitle;
		this.releaseDate = releaseDate;
		this.baseProduction = baseProduction;
		this.bookPrice = bookPrice;
	}



	public long getBookId() {
		return bookId;
	}

	public void setBookId(long bookId) {
		this.bookId = bookId;
	}
	
	public AuthorDTO getAuthorDTO() {
		return authorDTO;
	}


	public void setAuthorDTO(AuthorDTO authorDTO) {
		this.authorDTO = authorDTO;
	}


	public CategoryDTO getCategoryDTO() {
		return categoryDTO;
	}


	public void setCategoryDTO(CategoryDTO categoryDTO) {
		this.categoryDTO = categoryDTO;
	}


	public PublisherDTO getPublisherDTO() {
		return publisherDTO;
	}


	public void setPublisherDTO(PublisherDTO publisherDTO) {
		this.publisherDTO = publisherDTO;
	}


	public String getBookTitle() {
		return bookTitle;
	}

	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public Double getBaseProduction() {
		return baseProduction;
	}

	public void setBaseProduction(Double baseProduction) {
		this.baseProduction = baseProduction;
	}

	public Double getBookPrice() {
		return bookPrice;
	}

	public void setBookPrice(Double bookPrice) {
		this.bookPrice = bookPrice;
	}
	
}
