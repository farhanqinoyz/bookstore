package com.store.storebook.dtomodel;

public class PaperQualityDTO {
	private Long qualityId;
	private String qualityType;
	private Double paperPrice;
	
	public PaperQualityDTO(){
		
	}

	
	public PaperQualityDTO(Long qualityId, String qualityType, Double paperPrice) {
		super();
		this.qualityId = qualityId;
		this.qualityType = qualityType;
		this.paperPrice = paperPrice;
	}


	public String getQualityType() {
		return qualityType;
	}

	public void setQualityType(String qualityType) {
		this.qualityType = qualityType;
	}

	public Long getQualityId() {
		return qualityId;
	}

	public void setQualityId(Long qualityId) {
		this.qualityId = qualityId;
	}

	public Double getPaperPrice() {
		return paperPrice;
	}

	public void setPaperPrice(Double paperPrice) {
		this.paperPrice = paperPrice;
	}

	
	
}
