package com.store.storebook.dtomodel;

import com.store.storebook.model.PaperQuality;

public class PublisherDTO {

	private Long publisherId;
	private String city;
	private String country;
	private String publisherName;
	private String publisherAddress;
	private PaperQualityDTO paperQuality;
	
	public PublisherDTO(){
		
	}

	
	public PublisherDTO(Long publisherId, String city, String country, String publisherName, String publisherAddress,
			PaperQualityDTO paperQuality) {
		super();
		this.publisherId = publisherId;
		this.city = city;
		this.country = country;
		this.publisherName = publisherName;
		this.publisherAddress = publisherAddress;
		this.paperQuality = paperQuality;
	}




	public Long getPublisherId() {
		return publisherId;
	}

	public void setPublisherId(Long publisherId) {
		this.publisherId = publisherId;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPublisherName() {
		return publisherName;
	}

	public void setPublisherName(String publisherName) {
		this.publisherName = publisherName;
	}

	public String getPublisherAddress() {
		return publisherAddress;
	}

	public void setPublisherAddress(String publisherAddress) {
		this.publisherAddress = publisherAddress;
	}


	public PaperQualityDTO getPaperQuality() {
		return paperQuality;
	}


	public void setPaperQuality(PaperQualityDTO paperQuality) {
		this.paperQuality = paperQuality;
	}

	
	
}
