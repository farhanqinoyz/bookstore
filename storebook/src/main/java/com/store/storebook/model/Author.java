package com.store.storebook.model;
// Generated Jan 2, 2020 11:42:38 AM by Hibernate Tools 5.1.10.Final

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Author generated by hbm2java
 */
@Entity
@Table(name = "author", schema = "public")
public class Author implements java.io.Serializable {

	private long authorId;
	private Long age;
	private String country;
	private String firstName;
	private String lastName;
	private Set<Book> books = new HashSet<Book>(0);

	public Author() {
	}

	public Author(long authorId) {
		this.authorId = authorId;
	}

	public Author(long authorId, Long age, String country, String firstName, String lastName, Set<Book> books) {
		this.authorId = authorId;
		this.age = age;
		this.country = country;
		this.firstName = firstName;
		this.lastName = lastName;
		this.books = books;
	}

	@Id
	@Column(name = "author_id", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long getAuthorId() {
		return this.authorId;
	}

	public void setAuthorId(long authorId) {
		this.authorId = authorId;
	}

	@Column(name = "age")
	public Long getAge() {
		return this.age;
	}

	public void setAge(Long age) {
		this.age = age;
	}

	@Column(name = "country")
	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Column(name = "first_name")
	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name = "last_name")
	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "author")
	public Set<Book> getBooks() {
		return this.books;
	}

	public void setBooks(Set<Book> books) {
		this.books = books;
	}

}
