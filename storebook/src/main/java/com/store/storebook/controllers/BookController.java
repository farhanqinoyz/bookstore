package com.store.storebook.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.store.storebook.interfaces.BookProduction;
import com.store.storebook.model.*;
import com.store.storebook.dtomodel.*;
import com.store.storebook.repositories.*;

@RestController
@RequestMapping("api/book")
public class BookController implements BookProduction{
	
	@Autowired
	AuthorRepository authorRepository;
	
	@Autowired
	BookRepository bookRepository;
	
	@Autowired
	CategoryRepository categoryRepository;
	
	@Autowired
	PaperQualityRepository paperQualityRepository;
	
	@Autowired
	PublisherRepository publisherRepository;
	

	@GetMapping("/readAll")
	public Map<String, Object> getAllCategory(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<Book> listBookEntity = bookRepository.findAll();
		
		List<BookDTO> listBookDTO = new ArrayList<BookDTO>();
		for(Book book : listBookEntity) {
			BookDTO bookDTO = new BookDTO();
			AuthorDTO authorDTO = new AuthorDTO();
			CategoryDTO categoryDTO = new CategoryDTO();
			PaperQualityDTO paperQualityDTO = new PaperQualityDTO();
			PublisherDTO publisherDTO = new PublisherDTO();
			
			//Men-set Tabel Author
			authorDTO.setAuthorId(book.getAuthor().getAuthorId());
			authorDTO.setAge(book.getAuthor().getAge());
			authorDTO.setCountry(book.getAuthor().getCountry());
			authorDTO.setFirstName(book.getAuthor().getFirstName());
			authorDTO.setLastName(book.getAuthor().getLastName());
			bookDTO.setAuthorDTO(authorDTO);
			
			//Men-set Tabel Category
			categoryDTO.setCategoryId(book.getCategory().getCategoryId());
			categoryDTO.setCategoryName(book.getCategory().getCategoryName());
			bookDTO.setCategoryDTO(categoryDTO);
			
			//Men-set Tabel Publisher
			publisherDTO.setPublisherId(book.getPublisher().getPublisherId());
			publisherDTO.setPublisherName(book.getPublisher().getPublisherName());
			publisherDTO.setPublisherAddress(book.getPublisher().getPublisherAddress());
			publisherDTO.setCity(book.getPublisher().getCity());
			publisherDTO.setCountry(book.getPublisher().getCountry());
			paperQualityDTO.setQualityId(book.getPublisher().getPaperQuality().getQualityId());
			paperQualityDTO.setQualityType(book.getPublisher().getPaperQuality().getQualityType());
			paperQualityDTO.setPaperPrice(book.getPublisher().getPaperQuality().getPaperPrice());
			publisherDTO.setPaperQuality(paperQualityDTO);
			bookDTO.setPublisherDTO(publisherDTO);
			
			//Men-set Tabel Book (Utama)
			bookDTO.setBookId(book.getBookId());
			bookDTO.setBookTitle(book.getBookTitle());
			bookDTO.setReleaseDate(book.getReleaseDate());
			bookDTO.setBaseProduction(book.getBaseProduction());
			bookDTO.setBookPrice(book.getBookPrice());
			
			listBookDTO.add(bookDTO);
		}
		result.put("Status", 200); 
		result.put("Message", "Read data category success!");
		result.put("Data", listBookDTO);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	}
    	
		return result;
	}
	
	@PostMapping("/create")
	public Map<String, Object> createBook(@Valid @RequestBody BookDTO bookDTO){
		Map<String, Object> result = new HashMap<String, Object>();
		
			Book book = new Book();
			Category category = new Category();
			category.setCategoryId(bookDTO.getCategoryDTO().getCategoryId());
			
			Author author = new Author();
			author.setAuthorId(bookDTO.getAuthorDTO().getAuthorId());
			
	        Publisher publisher = new Publisher();
			publisher.setPublisherId(bookDTO.getPublisherDTO().getPublisherId());
			book.setAuthor(author);
			book.setPublisher(publisher);
			book.setCategory(category);
			book.setBookTitle(bookDTO.getBookTitle());
			book.setReleaseDate(bookDTO.getReleaseDate());
			book.setBaseProduction(bookDTO.getBaseProduction());
			book.setBookPrice(bookDTO.getBookPrice());
			
		result.put("Status", 200);
		result.put("Message", "Create new record Book success!");
		result.put("Data", bookRepository.save(book));
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	}
    	
		
		return result;
	}
	
}
