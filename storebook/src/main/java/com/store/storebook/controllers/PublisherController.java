package com.store.storebook.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.store.storebook.dtomodel.AuthorDTO;
import com.store.storebook.dtomodel.PaperQualityDTO;
import com.store.storebook.dtomodel.PublisherDTO;
import com.store.storebook.model.PaperQuality;
import com.store.storebook.model.Publisher;
import com.store.storebook.repositories.PublisherRepository;

@RestController
@RequestMapping("api/publisher")
public class PublisherController {
	@Autowired
	PublisherRepository publisherRepository;

	@GetMapping("/readAll")
	public Map<String, Object> getAllPublisher(){
		ModelMapper modelMapper = new ModelMapper();
		List<Publisher> listPublisher= publisherRepository.findAll();
		List<PublisherDTO> listPublisherDTO = new ArrayList();
		
		for(Publisher publisher : listPublisher) {
			PublisherDTO dto = modelMapper.map(publisher, PublisherDTO.class);
			listPublisherDTO.add(dto);
		}
			
		Map<String, Object> result = new HashMap<String, Object>();
		
		
		result.put("Status", 200);
		result.put("Message", "Read data category success!");
		result.put("Data", listPublisherDTO);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	
	@GetMapping("/getById/{id}")
	public Map<String, Object> getByIdPublisher(@PathVariable(value = "id") Long publisherId){
		ModelMapper modelMapper = new ModelMapper();
		Publisher publisher = publisherRepository.findById(publisherId).get();
		PublisherDTO dto = modelMapper.map(publisher, PublisherDTO.class);
		
		Map<String, Object> result = new HashMap<String, Object>();
		
		result.put("Status", 200);
		result.put("Message", "Read data category success!");
		result.put("Data", dto);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	
	@PostMapping("/create")
	public Map<String, Object> CreatePublisher(@Valid @RequestBody PublisherDTO publisherDTO){
		ModelMapper modelMapper = new ModelMapper();
			Publisher publisher = modelMapper.map(publisherDTO, Publisher.class);
			
		Map<String, Object> result = new HashMap<String, Object>();
		
		result.put("Status", 200);
		result.put("Message", "Read data category success!");
		result.put("Data", publisherRepository.save(publisher));
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	
	@PutMapping("/update/{id}")
	public Map<String, Object> updatePublisher(@Valid @RequestBody PublisherDTO publisherDTO,
			@PathVariable(value = "id") Long publisherId){

		Map<String, Object> result = new HashMap<String, Object>(); 
		ModelMapper modelMapper = new ModelMapper();
		Publisher publisherById = publisherRepository.findById(publisherId).get();
		publisherById = modelMapper.map(publisherDTO,Publisher.class);
		
		publisherById.setPublisherId(publisherId);
		
		publisherRepository.save(publisherById);
		result.put("Status", 200);
		result.put("Message", "Update data publisher success!");
		result.put("Data", publisherById);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	

	@DeleteMapping("/deleteById/{id}")
    public Map<String, Object> deletePublisherById(@PathVariable(value = "id") Long publisherId) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Publisher publisher = publisherRepository.findById(publisherId).get();
    	
        publisherRepository.delete(publisher);
        ResponseEntity.ok().build();
        
    	result.put("Status", 200);
		result.put("Message", "Delete a record category success!");
		result.put("ID : ",""+publisherId);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	}
    	
		
		return result;
    }
}
