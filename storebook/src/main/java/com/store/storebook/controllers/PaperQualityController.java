package com.store.storebook.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.store.storebook.dtomodel.AuthorDTO;
import com.store.storebook.dtomodel.PaperQualityDTO;
import com.store.storebook.model.PaperQuality;
import com.store.storebook.repositories.PaperQualityRepository;

@RestController
@RequestMapping("api/paperquality")

public class PaperQualityController {
	@Autowired
	PaperQualityRepository paperQualityRepository;
	
	@GetMapping("/readAll")
	public Map<String, Object> getAllPaperQuality(){
		ModelMapper modelMapper = new ModelMapper();
		List<PaperQuality> listPaperQuality = paperQualityRepository.findAll();
		List<PaperQualityDTO> listPaperQualityDTO = new ArrayList();
		
		for(PaperQuality paperQuality : listPaperQuality) {
			PaperQualityDTO dto = modelMapper.map(paperQuality, PaperQualityDTO.class);
			listPaperQualityDTO.add(dto);
		}
			
		Map<String, Object> result = new HashMap<String, Object>();
		
		
		result.put("Status", 200);
		result.put("Message", "Read data category success!");
		result.put("Data", listPaperQualityDTO);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	
	@GetMapping("/getById/{id}")
	public Map<String, Object> getByIdPaperQuality(@PathVariable(value = "id") Long qualityId){
		ModelMapper modelMapper = new ModelMapper();
		PaperQuality paperQuality = paperQualityRepository.findById(qualityId).get();
		AuthorDTO dto = modelMapper.map(paperQuality, AuthorDTO.class);
		
		Map<String, Object> result = new HashMap<String, Object>();
		
		result.put("Status", 200);
		result.put("Message", "Read data category success!");
		result.put("Data", dto);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	
	@PostMapping("/create")
	public Map<String, Object> CreatePaperQuality(@Valid @RequestBody PaperQualityDTO paperQualityDTO){
		ModelMapper modelMapper = new ModelMapper();
			PaperQuality paperQuality = modelMapper.map(paperQualityDTO, PaperQuality.class);
			
		Map<String, Object> result = new HashMap<String, Object>();
		
		result.put("Status", 200);
		result.put("Message", "Read data category success!");
		result.put("Data", paperQualityRepository.save(paperQuality));
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	
	@PutMapping("/update/{id}")
	public Map<String, Object> updateAuthor(@Valid @RequestBody PaperQualityDTO paperQualityDTO,
			@PathVariable(value = "id") Long qualityId){
		ModelMapper modelMapper = new ModelMapper();
		PaperQuality paperQualityById = paperQualityRepository.findById(qualityId).get();
		modelMapper.map(paperQualityDTO,paperQualityById);
		paperQualityById.setQualityId(qualityId);
		Map<String, Object> result = new HashMap<String, Object>();
		
		result.put("Status", 200);
		result.put("Message", "Read data category success!");
		result.put("Data", paperQualityRepository.save(paperQualityById));
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	
	@DeleteMapping("/deleteById/{id}")
    public Map<String, Object> deletePaperQualityById(@PathVariable(value = "id") Long qualityId) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	PaperQuality paperQuality = paperQualityRepository.findById(qualityId).get();
    	
        paperQualityRepository.delete(paperQuality);
        ResponseEntity.ok().build();
        
    	result.put("Status", 200);
		result.put("Message", "Delete a record category success!");
		result.put("ID : ",""+qualityId);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	}
    	
		
		return result;
    }
	
}
