package com.store.storebook.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.store.storebook.dtomodel.AuthorDTO;
import com.store.storebook.model.Author;
import com.store.storebook.model.Category;
import com.store.storebook.repositories.AuthorRepository;


@RestController
@RequestMapping("api/author")
public class AuthorController {

	@Autowired
	AuthorRepository authorRepository;
	
	@GetMapping("/readAll")
	public Map<String, Object> getAllAuthor(){
		ModelMapper modelMapper = new ModelMapper();
		List<Author> listAuthor = authorRepository.findAll();
		List<AuthorDTO> listAuthorDTO = new ArrayList();
		
		for(Author author : listAuthor) {
			AuthorDTO dto = modelMapper.map(author, AuthorDTO.class);
			listAuthorDTO.add(dto);
		}
			
		Map<String, Object> result = new HashMap<String, Object>();
		
		
		result.put("Status", 200);
		result.put("Message", "Read data category success!");
		result.put("Data", listAuthorDTO);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	
	@GetMapping("/getById/{id}")
	public Map<String, Object> getByIdAuthor(@PathVariable(value = "id") Long authorId){
		ModelMapper modelMapper = new ModelMapper();
		Author author = authorRepository.findById(authorId).get();
		AuthorDTO dto = modelMapper.map(author, AuthorDTO.class);
		
		Map<String, Object> result = new HashMap<String, Object>();
		
		result.put("Status", 200);
		result.put("Message", "Read data category success!");
		result.put("Data", dto);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	
	@PostMapping("/create")
	public Map<String, Object> CreateAuthor(@Valid @RequestBody AuthorDTO authorDTO){
		ModelMapper modelMapper = new ModelMapper();
			Author author = modelMapper.map(authorDTO, Author.class);
			
			
		Map<String, Object> result = new HashMap<String, Object>();
		
		
		result.put("Status", 200);
		result.put("Message", "Read data category success!");
		result.put("Data", authorRepository.save(author));
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	
	@PutMapping("/update/{id}")
	public Map<String, Object> updateAuthor(@Valid @RequestBody AuthorDTO authorDTO,
			@PathVariable(value = "id") Long authorId){
		ModelMapper modelMapper = new ModelMapper();
		Author authorById = authorRepository.findById(authorId).get();
		modelMapper.map(authorDTO,authorById);
		authorById.setAuthorId(authorId);
		Map<String, Object> result = new HashMap<String, Object>();
		
		result.put("Status", 200);
		result.put("Message", "Read data category success!");
		result.put("Data", authorRepository.save(authorById));
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	    
		}
		return result;
	}
	
	// Get a Delete Author
	@DeleteMapping("/deleteById/{id}")
    public Map<String, Object> deleteCategoryById(@PathVariable(value = "id") Long authorId) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Author author = authorRepository.findById(authorId).get();
    	
        authorRepository.delete(author);
        ResponseEntity.ok().build();
        
    	result.put("Status", 200);
		result.put("Message", "Delete a record category success!");
		result.put("ID : ",""+authorId);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	}
    	
		
		return result;
    }
}
