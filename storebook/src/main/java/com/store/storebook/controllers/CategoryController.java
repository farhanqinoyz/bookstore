package com.store.storebook.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.store.storebook.dtomodel.CategoryDTO;
import com.store.storebook.model.Category;
import com.store.storebook.repositories.CategoryRepository;

@RestController
@RequestMapping("api/category")
public class CategoryController {
	@Autowired
	CategoryRepository categoryRepository;
	
	@GetMapping("/readAll")
	public Map<String, Object> getAllPaperQuality(){
		Map<String, Object> result = new HashMap<String, Object>();
		List<Category> listCategoryEntity = categoryRepository.findAll();
		
		List<CategoryDTO> listCategoryDTO = new ArrayList<CategoryDTO>();
		for(Category category : listCategoryEntity) {
			CategoryDTO categoryDTO = new CategoryDTO();
			categoryDTO.setCategoryId(category.getCategoryId());
			categoryDTO.setCategoryName(category.getCategoryName());
			listCategoryDTO.add(categoryDTO);
		}
		result.put("Status", 200);
		result.put("Message", "Read data category success!");
		result.put("Data", listCategoryDTO);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	}
		return result;
	}
	
	// Get a Single Category
    @GetMapping("/getById/{id}")
    public Map<String, Object> getCategoryById(@PathVariable(value = "id") Long categoryId) {
    	Map<String, Object> result = new HashMap<String, Object>();
		
    	result.put("Status", 200);
		result.put("Message", "Create new record category success!");
		result.put("Data", categoryRepository.findById(categoryId).get());
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	}
    	
		
		return result;
    }
    
	
	@PostMapping("/create")
	public Map<String, Object> createCategory(@Valid @RequestBody CategoryDTO categoryDTO){
		Map<String, Object> result = new HashMap<String, Object>();
		
			Category category = new Category();
			category.setCategoryName(categoryDTO.getCategoryName());
		
		result.put("Status", 200);
		result.put("Message", "Create new record category success!");
		result.put("Data", categoryRepository.save(category));
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	}
    	
		
		return result;
	}
	
	@PutMapping("/edit/{id}")
	public Map<String, Object> editCategory(@PathVariable(value = "id") Long categoryId, 
			@Valid @RequestBody CategoryDTO categoryDTO){
		Map<String, Object> result = new HashMap<String, Object>();
		
		
			Category category = categoryRepository.findById(categoryId).get();
			category.setCategoryName(categoryDTO.getCategoryName());
			
	        Category updatedCategory = categoryRepository.save(category);	
			
			
		result.put("Status", 200);
		result.put("Message", "Edited record by ID : "+categoryId+", category success!");
		result.put("Data", updatedCategory);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	}
    	
		
		return result;
	}
	
	// Get a Delete Category
    @DeleteMapping("/deleteById/{id}")
    public Map<String, Object> deleteCategoryById(@PathVariable(value = "id") Long categoryId) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Category category = categoryRepository.findById(categoryId).get();
    	
        categoryRepository.delete(category);
        ResponseEntity.ok().build();
        
    	result.put("Status", 200);
		result.put("Message", "Delete a record category success!");
		result.put("ID : ",""+categoryId);
		
		for (String key: result.keySet()) {
    	    System.out.println(key + " : " + result	.get(key));
    	}
    	
		
		return result;
    }
}
